package willcodeforfood.bookmoods;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by MAHE on 24-Apr-16.
 */
public class CreateTables extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "BooksDB.db";

    public CreateTables(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL( "drop database BooksDB");
        ///db.execSQL(TableDefinition.SQL_DELETE_ENTRIES);
        System.out.println ("books dropped");
        db.execSQL(TableDefinition.SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(TableDefinition.SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}

