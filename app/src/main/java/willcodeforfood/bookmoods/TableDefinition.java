package willcodeforfood.bookmoods;

import android.provider.BaseColumns;

/**
 * Created by MAHE on 24-Apr-16.
 */
public class TableDefinition {
    TableDefinition() {};
    public static abstract class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "books";
        public static final String COLUMN_NAME_BOOK_NAME = "book_name";
        public static final String COLUMN_NAME_ISBN = "isbn";
        public static final String COLUMN_NAME_SAD = "sad";
        public static final String COLUMN_NAME_CHEERFUL = "cheerful";
        public static final String COLUMN_NAME_DARK = "dark";
        public static final String COLUMN_NAME_THRILLING = "thrilling";
        public static final String COLUMN_NAME_HUMOUROUS = "humourous";
        public static final String COLUMN_NAME_ROMANTIC = "romantic";
        public static final String COLUMN_NAME_ANGER = "anger";
        public static final String COLUMN_NAME_SHAME = "shame";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";
    private static final String COMMA = ",";
    static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +
                    FeedEntry.COLUMN_NAME_ISBN + INT_TYPE + COMMA +
                    FeedEntry.COLUMN_NAME_BOOK_NAME + TEXT_TYPE + " PRIMARY KEY" + COMMA +
                    FeedEntry.COLUMN_NAME_SAD + INT_TYPE + COMMA +
                    FeedEntry.COLUMN_NAME_CHEERFUL + INT_TYPE + COMMA +
                    FeedEntry.COLUMN_NAME_DARK + INT_TYPE + COMMA +
                    FeedEntry.COLUMN_NAME_THRILLING + INT_TYPE + COMMA +
                    FeedEntry.COLUMN_NAME_HUMOUROUS + INT_TYPE + COMMA +
                    FeedEntry.COLUMN_NAME_ROMANTIC + INT_TYPE + COMMA +
                    FeedEntry.COLUMN_NAME_ANGER + INT_TYPE + COMMA +
                    FeedEntry.COLUMN_NAME_SHAME + INT_TYPE +
            " )";
    static final String SQL_DELETE_ENTRIES =
            "DROP TABLE " + FeedEntry.TABLE_NAME;

}
