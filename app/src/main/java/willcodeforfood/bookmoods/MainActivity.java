package willcodeforfood.bookmoods;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hod.api.hodclient.HODApps;
import hod.api.hodclient.HODClient;
import hod.api.hodclient.IHODClientCallback;
import hod.response.parser.HODResponseParser;
import hod.response.parser.SentimentAnalysisResponse;


public class MainActivity extends AppCompatActivity implements IHODClientCallback {

    HODClient hodClient;
    HODResponseParser hodParser;
    CreateTables create;
    private final String apikey="c10fc0f6-e2ff-4260-a48d-fba32771b4dd";
    String moods[] = {"Sad", "Cheerful", "Dark", "Thrilling", "Humourous", "Romantic", "Anger", "Shame"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        create = new CreateTables(getBaseContext());

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Tab 1"));
        tabLayout.addTab(tabLayout.newTab().setText("Tab 2"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        hodClient=new HODClient(apikey,this);
        hodParser=new HODResponseParser();

    }
    private void useHODClient(String text) {
        String hodApp = HODApps.ANALYZE_SENTIMENT;
        Map<String, Object> params = new HashMap<String, Object>();
        List<String> urls = new ArrayList<String>();
        urls.add(text);
        params.put("text", urls);
        hodClient.PostRequest(params, hodApp, HODClient.REQ_MODE.SYNC);
    }
    private void connectDB(SentimentAnalysisResponse obj){
        int l=0;
        l=obj.positive.size();
        String sents[]=new String [100];
        double vals[]=new double [100];
        int i=0;
        while(l>0) {
            vals[i] = obj.positive.get(l-1).score;
            sents[i] = obj.positive.get(l-1).sentiment;

            i++;
            l--;
        }
        l=obj.negative.size();
        while(l>0) {
            sents[i] = obj.negative.get(l-1).sentiment;
            vals[i]= obj.negative.get(l-1).score;
            i++;
            l--;
        }
        for (int k=0;k<obj.positive.size()+obj.negative.size();k++) {
            String tokenised [] = sents[k].split(" ");
            int ij=0;

            for (; ij<tokenised.length; ij++) {
                ThesaurusFetch thesaurusFetch = new ThesaurusFetch();
                thesaurusFetch.execute(tokenised[ij]);
            }
            //System.out.println (tokenised [ij]);

        }

    }
    void calcMoodValues(String synonyms)
    {
        System.out.println("Reached calcMoodValues");
    }

    private void predictionEntry(int moodValues[])
    {
        SQLiteDatabase db = create.getWritableDatabase();
        ContentValues values = new ContentValues();

        //int moodValues[] = new int [8];

        getMoodValues(moodValues);

        //random initialiser for mood
        for (int i=0; i<moodValues.length; i++)
            moodValues[i]=i*10;
        // Create a new map of values, where column names are the keys
        values.put(TableDefinition.FeedEntry.COLUMN_NAME_BOOK_NAME, "Robin Hood 2");
        values.put(TableDefinition.FeedEntry.COLUMN_NAME_ISBN, 1234);
        values.put(TableDefinition.FeedEntry.COLUMN_NAME_SAD, moodValues[0]);
        values.put(TableDefinition.FeedEntry.COLUMN_NAME_CHEERFUL, moodValues[1]);
        values.put(TableDefinition.FeedEntry.COLUMN_NAME_DARK, moodValues[2]);
        values.put(TableDefinition.FeedEntry.COLUMN_NAME_THRILLING, moodValues[3]);
        values.put(TableDefinition.FeedEntry.COLUMN_NAME_HUMOUROUS, moodValues[4]);
        values.put(TableDefinition.FeedEntry.COLUMN_NAME_ROMANTIC, moodValues[5]);
        values.put(TableDefinition.FeedEntry.COLUMN_NAME_ANGER, moodValues[6]);
        values.put(TableDefinition.FeedEntry.COLUMN_NAME_SHAME, moodValues[7]);


// Insert the new row, returning the primary key value of the new row
        long newRowId;
        newRowId = db.insert(
                TableDefinition.FeedEntry.TABLE_NAME,
                null,
                values);
        System.out.println("Row added");
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();


        db = create.getReadableDatabase();

// Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                TableDefinition.FeedEntry.COLUMN_NAME_ISBN,
                TableDefinition.FeedEntry.COLUMN_NAME_BOOK_NAME,
                TableDefinition.FeedEntry.COLUMN_NAME_SAD,
                TableDefinition.FeedEntry.COLUMN_NAME_CHEERFUL,
                TableDefinition.FeedEntry.COLUMN_NAME_DARK,
                TableDefinition.FeedEntry.COLUMN_NAME_THRILLING,
                TableDefinition.FeedEntry.COLUMN_NAME_HUMOUROUS,
                TableDefinition.FeedEntry.COLUMN_NAME_ROMANTIC,
                TableDefinition.FeedEntry.COLUMN_NAME_ANGER,
                TableDefinition.FeedEntry.COLUMN_NAME_SHAME
        };

// How you want the results sorted in the resulting Cursor
        //String sortOrder = TableDefinition.FeedEntry.COLUMN_NAME_UPDATED + " DESC";

        Cursor cursor = db.query(
                TableDefinition.FeedEntry.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null);                                 // The sort order

        String bestItem = "Initial";
        System.out.println(bestItem+"2");
        int lowestDiff = 800;
        int parameter[] = {50, 50, 50, 50, 50, 50, 50, 50};
        while (cursor.moveToNext())
        {
            String item = cursor.getString(cursor.getColumnIndexOrThrow(TableDefinition.FeedEntry.COLUMN_NAME_BOOK_NAME));
            System.out.println(item);
            int totalDiff = 0;
            totalDiff += Math.abs (parameter[0]-cursor.getInt(cursor.getColumnIndexOrThrow(TableDefinition.FeedEntry.COLUMN_NAME_SAD)));
            totalDiff += Math.abs (parameter[1]-cursor.getInt(cursor.getColumnIndexOrThrow(TableDefinition.FeedEntry.COLUMN_NAME_CHEERFUL)));
            totalDiff += Math.abs (parameter[2]-cursor.getInt(cursor.getColumnIndexOrThrow(TableDefinition.FeedEntry.COLUMN_NAME_DARK)));
            totalDiff += Math.abs (parameter[3]-cursor.getInt(cursor.getColumnIndexOrThrow(TableDefinition.FeedEntry.COLUMN_NAME_THRILLING)));
            totalDiff += Math.abs (parameter[4]-cursor.getInt(cursor.getColumnIndexOrThrow(TableDefinition.FeedEntry.COLUMN_NAME_HUMOUROUS)));
            totalDiff += Math.abs (parameter[5]-cursor.getInt(cursor.getColumnIndexOrThrow(TableDefinition.FeedEntry.COLUMN_NAME_ROMANTIC)));
            totalDiff += Math.abs (parameter[6]-cursor.getInt(cursor.getColumnIndexOrThrow(TableDefinition.FeedEntry.COLUMN_NAME_ANGER)));
            totalDiff += Math.abs (parameter[7]-cursor.getInt(cursor.getColumnIndexOrThrow(TableDefinition.FeedEntry.COLUMN_NAME_SHAME)));
            if (totalDiff<lowestDiff)
            {
                lowestDiff=totalDiff;
                bestItem= item;
            }
        }
        System.out.println(bestItem);
    }
    private void getMoodValues(int moodValues[])
    {
        //call api
        //a loop for different sentiments

    }

    public void requestCompletedWithContent(String response) {
        SentimentAnalysisResponse obj= hodParser.ParseSentimentAnalysisResponse(response);
        connectDB(obj);
    }
    @Override
    public void onErrorOccurred(String errorMessage) {
        // handle error if any
        System.out.println(errorMessage);
    }
    @Override
    public void requestCompletedWithJobID(String response) {

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void startAnalysis(View v)
    {
        EditText editText=(EditText)findViewById(R.id.textentry);
        String text=editText.getText().toString();
        useHODClient(text);

    }
    public void startAnalysis2(View v)
    {
        double moodArr[]=new double[8];
        SeekBar seekBar=(SeekBar)findViewById(R.id.sadbar);
        moodArr[0]=seekBar.getProgress();
        seekBar=(SeekBar)findViewById(R.id.cheerfulbar);
        moodArr[1]=seekBar.getProgress();
        seekBar=(SeekBar)findViewById(R.id.darkbar);
        moodArr[2]=seekBar.getProgress();
        seekBar=(SeekBar)findViewById(R.id.thrillingbar);
        moodArr[3]=seekBar.getProgress();
        seekBar=(SeekBar)findViewById(R.id.humorousbar);
        moodArr[4]=seekBar.getProgress();
        seekBar=(SeekBar)findViewById(R.id.romanticbar);
        moodArr[5]=seekBar.getProgress();
        seekBar=(SeekBar)findViewById(R.id.angrybar);
        moodArr[6]=seekBar.getProgress();
        seekBar=(SeekBar)findViewById(R.id.embarassingbar);
        moodArr[7]=seekBar.getProgress();


    }
}
    class ThesaurusFetch extends AsyncTask<String,Void,String> {



        protected String doInBackground(String... k)
        {

            HttpURLConnection urlConnection = null;

            try {
                String apikey2 = "f88b9ee3bc9921f5e3db90b371423827";
                URL url = new URL("http://words.bighugelabs.com/api/2/" + apikey2 + "/" + k[0].toString() + "/json");
                System.out.println("http://words.bighugelabs.com/api/2/" + apikey2 + "/" + k[0].toString() + "/json");
                urlConnection = (HttpURLConnection) url.openConnection();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String stringBuilder = new String();
                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    stringBuilder+=line;
                    //System.out.println(stringBuilder);
                }
                bufferedReader.close();
                return stringBuilder;
            } catch (FileNotFoundException e)
            {
                System.out.println("File not found");
                return null;
            }
            catch (Exception e) {
                return null;

            } finally {
                if (urlConnection!=null)
                urlConnection.disconnect();

            }
        }

        @Override



        protected void onPostExecute(String response){
            System.out.println(response);

        }
    }